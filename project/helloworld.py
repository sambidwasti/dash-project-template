""" HelloWorld Script"""
# Imports
import math

def helloworld():
    """ Prints helloworld.
    """
    print('helloworld')

def askworld(question = "What is the meaning of Life?"):
    """ An example of taking in parameters and returning a variable.
    
    Parameters
    ----------
    question : str
        A question to ask the world.

    Returns
    -------
    answer : str
        Answer from the Universe.
    """
    answer = "I do not know. I am pretty dumb. It is up to you to figure it out."
    return(answer)

if __name__ == '__main__':
    """ This runs when the script is ran or else it is avoided. """
    helloworld()