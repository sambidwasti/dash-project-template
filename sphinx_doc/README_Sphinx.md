# Python Project
This is a repository of the Python Project.

## Running the app  ##
```bash
python app.py
python helloworld.py
```
## Installation ##
Currently, we do not have a setup. We are just running the repo. Ideally (MAYBE), we would install via the following
>**TODO TASK**: Create a setup file
```bash
pip3 install . 
```

## Documentation Style

### Code
Of course abide by [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the exception
of maximum line lengths. We are using 90 characters as our maximum line length. For bonus
points, it is recommended that code is formatted with
[python black](https://pypi.org/project/black/).


### Docstrings
We are attempting to adhere to the
[numpy docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html) or 
[google docstring guide](https://google.github.io/styleguide/pyguide.html)

The differences can be seen here:
[google doc string](https://bwanamarko.alwaysdata.net/napoleon/format_exception.html)


## Sphinx Documentation
For the ease of working, various executable scripts (for each steps)
have been created. Execute these scripts in order. Some modification
might be needed but ideally, it should not. 

### Steps and Description      

**Running basic scripts**    

There are 3 scripts and you can run these in order. The ```00-sphinx``` script has all 3 scripts in one.
If you are just adding a script, quick edits, this should be good enough. 

* 01-sphinx: Initializes sphinx-apidoc
  * With the full build (F) module is not created. So we rerun the command twice. 
  Once with the F and once without it.
  
* 02-sphinx: Copies, replaces and removes the config and index files. 
  
* 03-sphinx: Creates the html. (by using ``` make html ``` )
  * The folder is in /_build/html.
  * Errors/Warnings with sphinx will be displayed here. 
  * Click on the index.html and verify documentation is ok.    
        


* Few Debugs:       
  
  * If it can't find the documentation at all, it's usually an issue with the path. 
    The "path" is defined in the conf.py
     >**Note: The edits to conf.py, index.txt and README_Sphinx.md should be done to the one in _temp_files.
      > Since we are working with the apidoc, these files will replace the sphinx default files.**
  * Also check the modules or respective module's ```*.rst``` file to verify the path.
  * Any file with some issue will not be documented.
    Need to look at the warning. In the Git-pipeline this might be due to missing pip install package.
  * If the documentation is not correct, there might be some issue
    with spaces and tabs. Sphinx is really picky. So usually, i just
    copy-paste the working section and edit it rather than trying to 
    figure out what exactly is the issue.
  * After every edit, re-run 03-sphinx / or to be thorough run 00-sphinx. When you refresh the page
    you should see the changes. 

### Gitlab pages

For the gitlab, nothing needs to be done. The ```.gitlab-ci.yml``` already has the command needed there.
The CI/CD pipeline named ```pages``` should take care of auto-documentation.        

---
>**NOTE: If you do not see your script, usually there is some issue with the script which does not 
>necessarily provide an issue in the CI/CD pipeline.**

